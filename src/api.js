
import base64 from 'react-native-base64';
export const loginIn = (login, password) => {
          fetch("http://localhost:5000/login" , {  
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Basic ' + base64.encode(login + ":" + password),
    })}).then(response => response.json()).then(data => {return data.token}).catch(error => {console.log(error)});
}
export const getMoviesFromApi = () => {
    return fetch('https://reactnative.dev/movies.json')
      .then((response) => response.json())
      .then((json) => {
        return json.movies;
      })
      .catch((error) => {
        console.error(error);
      });
  };