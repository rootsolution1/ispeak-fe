export const BACK_COLOR = "#E6E6FA";
export const BUTTON_COLOR = "#4B0082";
export const INPUT_COLOR = "#9370DB";
export const LOGO_COLOR = "#800080";
export const HEADER_COLOR = "#4B0082";