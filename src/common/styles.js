import { StyleSheet } from 'react-native'
import {BACK_COLOR,LOGO_COLOR,INPUT_COLOR,BUTTON_COLOR} from "./types";
export const globalStyles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: BACK_COLOR,
      alignItems: 'center',
      justifyContent: 'center',
    },
    loginText:{
      color:"white"
    },
    logo:{
        fontWeight:"bold",
        fontSize:50,
        color:LOGO_COLOR,
        marginBottom:40
      },
      inputView:{
        width:"80%",
        backgroundColor:INPUT_COLOR,
        borderRadius:25,
        height:50,
        marginBottom:20,
        justifyContent:"center",
        padding:20
      },
      inputText:{
        height:50,
        color:"white"
      },
      darkText: {
        color:BUTTON_COLOR,
        fontSize:22,
      }
  });