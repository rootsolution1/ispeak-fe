import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import NetInfo from "@react-native-community/netinfo";
import Navigator from './Navigator';
import NoConnectionScreen from './components/general/NoConnectionScreen';
export default class Main extends React.Component {
    state = {
      connection : false,
    }
    componentDidMount(){
      const unsubscribe = NetInfo.addEventListener(state => {this.setState({connection : state.isConnected})});
    }
    render(){
      if (!this.state.connection){
        return <NoConnectionScreen/>
      }
      return(
      <NavigationContainer>
          <Navigator/>
      </NavigationContainer>
      )
    }
}