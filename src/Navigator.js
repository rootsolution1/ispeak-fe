import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import MainStack from './components/mainflow/MainStack';
import LoginStack from './components/login/LoginStack';


const Stack = createStackNavigator();

function Navigator() {
  return (
    <Stack.Navigator
    initialRouteName={"LoginStack"}
    headerMode={"none"}>
      <Stack.Screen name="LoginStack" component={LoginStack} />
      <Stack.Screen name="MainStack" component={MainStack}/>
    </Stack.Navigator>
  );
}
export default Navigator;