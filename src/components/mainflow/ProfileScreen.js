import React from 'react';
import {Text, View,Image, StyleSheet,ImageBackground, Dimensions,TouchableOpacity} from 'react-native';
import {BACK_COLOR, BUTTON_COLOR, INPUT_COLOR } from '../../common/types';
import {ProgressChart} from "react-native-chart-kit";
export default class ProfileScreen extends React.Component {
    state= {
        data : {
            labels: ["Science", "Literature", "General"], // optional
            data: [0.4, 0.6, 0.8]
          },
          chartConfig : {
            backgroundColor: BACK_COLOR,
            backgroundGradientFrom: BACK_COLOR,
            backgroundGradientTo: BACK_COLOR,
            color: (opacity = 1) => `rgba(75, 0, 130, ${opacity})`,
            strokeWidth: 2, // optional, default 3
            barPercentage: 0.5,
            useShadowColorFromDataset: false // optional
          },
    }
      render(){
        return (
          <View style={{backgroundColor:BACK_COLOR,width:'100%',height:'100%'}}>
            <View style={{height:250,width:"100%",bac:INPUT_COLOR,alignItems:'center',justifyContent:'flex-end', borderRadius:15}}>
               <ImageBackground style={{height:250,width:"100%",bac:INPUT_COLOR,alignItems:'center',justifyContent:'flex-end'}} source={{uri:"https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg"}}>
                <Image style={styles.image} source={{uri:"https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg"}}/>
                <Text style={styles.username}>Cool Guy</Text>
                </ImageBackground>
            </View>
            <View>
                <View style={{paddingLeft:"20%",paddingTop:"5%",paddingBottom:'10%',flex:1,flexDirection:'row',width:'100%'}}>
                    <Text style={styles.language}>Learning language: English</Text>
                </View>
                <ProgressChart
                    data={this.state.data}
                    width={Dimensions.get('window').width* 0.8}
                    height={220}
                    strokeWidth={16}
                    radius={32}
                    chartConfig={this.state.chartConfig}
                    hideLegend={false}/>
            </View>
            <View style={{flexDirection:'row',flex:1,paddingLeft:'10%'}}>
            <TouchableOpacity style={styles.button}>
              <Text style={styles.text}>Edit profile</Text>
            </TouchableOpacity>
            <TouchableOpacity style= {{width:'10%'}}/>
            <TouchableOpacity style={styles.button}>
              <Text style={styles.text}>Logout</Text>
            </TouchableOpacity>
            </View>
          </View>
        );
      }
    }
    
    const styles = StyleSheet.create({
        image:{
            height:120,
            width:120,
            borderRadius:120,
            marginBottom:20,
        },
        username:{
            fontSize:28,
            color:'white',
            fontFamily:'Serif',
            fontWeight:"bold",
            marginBottom:20,
        },
        language: {
            color:BUTTON_COLOR,
            fontSize:22,
            fontFamily:'serif',
            fontWeight:"bold",
            paddingLeft:"5%"
        },
        button:{
            width:"40%",
            backgroundColor:BUTTON_COLOR,
            borderRadius:25,
            height:50,
            alignItems:"center",
            justifyContent:"center",
            marginTop:20,
          },
          text:{
            color:"white"
          }
    })