import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import React from 'react';
import ContactScreen from './ContactScreen';
import MainScreen from './MainScreen';
import ProfileScreen from './ProfileScreen';
import SettingsScreen from './SettingsScreen';
import { BUTTON_COLOR, LOGO_COLOR } from '../../common/types';
import Icon from 'react-native-vector-icons/Ionicons';
const Tab = createBottomTabNavigator();

function MainStack() {
  return (
    <Tab.Navigator
    screenOptions={({ route }) => ({
      tabBarIcon: ({ focused, color, size }) => {
        let iconName;
        if (route.name === 'Learning') {
          iconName = focused
            ? 'chatbox-ellipses'
            : 'chatbox-ellipses-outline';
        } else if (route.name === 'Settings') {
          iconName = focused ? 'settings' : 'settings-outline';
        }
        else if (route.name === "Contacts"){
          iconName = focused ? 'people' : 'people-outline';
        }
        else if (route.name === "Profile") {
          iconName = focused ? "person" : "person-outline"
        }
        // You can return any component that you like here!
        return <Icon name={iconName} size={size} color={color} />;
      },
    })}
    tabBarOptions={{
      activeTintColor: BUTTON_COLOR,
      inactiveTintColor: "gray",
    }}
    initialRouteName={"Learning"}>
      <Tab.Screen name="Profile" component={ProfileScreen}/>
      <Tab.Screen name="Learning" component={MainScreen} />
      <Tab.Screen name="Contacts" component={ContactScreen}/>
      <Tab.Screen name="Settings" component={SettingsScreen}/>
    </Tab.Navigator>
  );
}
export default MainStack;