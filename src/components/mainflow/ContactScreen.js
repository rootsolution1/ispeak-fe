import React from 'react';
import {Text, View, ActivityIndicator, StyleSheet, Image} from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import {globalStyles} from '../../common/styles';
import {BUTTON_COLOR, HEADER_COLOR, INPUT_COLOR} from '../../common/types';
import Icon from 'react-native-vector-icons/Ionicons';
export default class ContactScreen extends React.Component {
  state={
    isLoading:false,
    contacts: [{name:"Username",logo:"https://www.t-nation.com/system/publishing/articles/10005529/original/6-Reasons-You-Should-Never-Open-a-Gym.png"},{name:"Username",logo:"https://www.t-nation.com/system/publishing/articles/10005529/original/6-Reasons-You-Should-Never-Open-a-Gym.png"},{name:"Username",logo:"https://www.t-nation.com/system/publishing/articles/10005529/original/6-Reasons-You-Should-Never-Open-a-Gym.png"}],
  }
  renderItem({item}){
    return (
      <View style={styles.contactContainer}>
          <Image source={{ uri:item.logo }} style={styles.profileImg}/>
          <View style={{flex:1,flexDirection:'row',alignItems:'center'}}>
            <Text style={{fontSize:18,color:"white",paddingLeft:"5%"}}>{item.name}</Text>
            <Icon name={"pencil"} color={BUTTON_COLOR} size={20} onPress={() => {console.log("Hello")}}/>
            <Icon name={"trash"} color={BUTTON_COLOR} size={20} onPress={() => {console.log("Hello")}}/>
          </View>
      </View>
    )
  }
      render(){
        return (
          <View style={globalStyles.container}>
            <View style={styles.header}>
              <Text style={styles.headerText}>My Contacts</Text>
              <Icon name={"search"} color={"white"} size={30} onPress = {() => {console.log("Hello")}}/>
            </View>
            {this.state.isLoading ?
            <View style={globalStyles.container}>
              <ActivityIndicator size="large" color={BUTTON_COLOR} />
            </View> :
            <FlatList 
              data={this.state.contacts}
              style={{
                width: '100%',paddingLeft:'5%',paddingTop:'10%'}}
              renderItem={this.renderItem}
              ItemSeparatorComponent={
                () => <View style={{ height: 8 }}/>
              }
              ListEmptyComponent= { () => {
              <View style={globalStyles.container}>
                <Text style={globalStyles.darkText}>There is no contacts</Text>
              </View>}}
            /> }
          </View>
        );
      }
    }
    const styles = StyleSheet.create({
      profileImg: {
        height: 40,
        width: 40,
        borderRadius: 40,
        overflow: 'hidden'
      },
      contactContainer: {
        minHeight:40,
        padding:5,
        width: '90%',
        flex: 1, 
        flexDirection: 'row',
        backgroundColor: INPUT_COLOR,
      },
      header: {
        backgroundColor:HEADER_COLOR,
        height:50,
        width:'100%',
        justifyContent:'center',
        alignItems:'center',
        flexDirection:'row',
      },
      headerText: {
        color:'white',
        fontSize:20,
        fontWeight:"bold",
      }
    }
    )
