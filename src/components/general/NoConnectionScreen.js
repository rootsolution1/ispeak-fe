import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator} from 'react-native';
import {BACK_COLOR,BUTTON_COLOR} from "../../common/types";
export default class NoConnectionScreen extends React.Component {
      render(){
        return (
          <View style={styles.container}>
              <Text style={styles.loginText}>Connection troubles</Text>
            <ActivityIndicator size="large" color={BUTTON_COLOR} />
          </View>
        );
      }
    }
    
    const styles = StyleSheet.create({
      container: {
        flex: 1,
        backgroundColor: BACK_COLOR,
        alignItems: 'center',
        justifyContent: 'center',
      },
      loginText:{
        color:BUTTON_COLOR
      }
    });