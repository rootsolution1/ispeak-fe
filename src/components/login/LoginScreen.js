import React from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity,Alert } from 'react-native';
import { loginIn} from "../../api";
import {BUTTON_COLOR} from "../../common/types";

import DialogInput from 'react-native-dialog-input';
import { globalStyles } from '../../common/styles';
export default class LoginScreen extends React.Component {
      state={
        login:"",
        password:"",
        dialogVisible:false,
      }
      onPressLogin = () => {
        // let token = loginIn(this.state.login,this.state.password)
        // if (token){
        //   AsyncStorage.setItem("@user_token",token).then(() => {this.props.navigation.navigate("MainStack",{ screen: 'Main' })})
        // }
        this.props.navigation.navigate("MainStack",{ screen: 'Main' });
      }
      onPressSignUp = () => {
        this.props.navigation.navigate("SingUp")
      }
      forgotPassword = () => {
        this.setState({dialogVisible:true})
      }
 
      handleCancel = () => {
        this.setState({dialogVisible:false})
      };
     
      handleSend = (text) => {
        // The user has pressed the "Delete" button, so here you can do your own logic.
        // ...Your logic
        console.log(text)
        this.setState({dialogVisible:false})
      };
      render(){
        return (
          <View style={globalStyles.container}>
            <Text style={globalStyles.logo}>iSpeak</Text>
            <View style={globalStyles.inputView} >
              <TextInput  
                style={globalStyles.inputText}
                placeholder="Login..." 
                placeholderTextColor="#003f5c"
                onChangeText={text => this.setState({login:text})}/>
            </View>
            <View style={globalStyles.inputView} >
              <TextInput  
                secureTextEntry
                style={globalStyles.inputText}
                placeholder="Password..." 
                placeholderTextColor="#003f5c"
                onChangeText={text => this.setState({password:text})}/>
            </View>
            <TouchableOpacity onPress={this.forgotPassword}>
              <Text style={styles.forgot}>Forgot Password?</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.loginBtn} onPress={this.onPressLogin}>
              <Text style={styles.loginText}>Login</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.loginBtn} onPress={this.onPressSignUp}>
              <Text style={styles.loginText}>Sign Up</Text>
            </TouchableOpacity>
            {this.state.dialogVisible && 
                <DialogInput
                  title={"Forgot password"}
                  message={"Enter your email to restore password"}
                  hintInput ={"Email"}
                  submitInput={ (inputText) => {this.handleSend(inputText)} }
                  closeDialog={this.handleCancel}>
                </DialogInput>}
          </View>
        );
      }
    }
    
    const styles = StyleSheet.create({
      forgot:{
        color:"black",
        fontSize:11
      },
      loginBtn:{
        width:"80%",
        backgroundColor:BUTTON_COLOR,
        borderRadius:25,
        height:50,
        alignItems:"center",
        justifyContent:"center",
        marginTop:20,
      },
      loginText:{
        color:"white"
      }
    });