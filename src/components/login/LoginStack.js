import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import LoginScreen from "./LoginScreen";
import SignUpScreen from "./SignUpScreen";

const Stack = createStackNavigator();

function LoginStack() {
  return (
    <Stack.Navigator
    initialRouteName={"Login"}
    headerMode={"none"}>
      <Stack.Screen name="Login" component={LoginScreen} />
      <Stack.Screen name="SingUp" component={SignUpScreen}/>
    </Stack.Navigator>
  );
}
export default LoginStack;