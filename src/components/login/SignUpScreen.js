import React from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native';
import {BUTTON_COLOR} from "../../common/types";
import {globalStyles} from '../../common/styles';
export default class SignUpScreen extends React.Component {
      state={
        login:"",
        password:""
      }
      onPress = () => {
        this.props.navigation.navigate("Login")
      }
      render(){
        return (
          <View style={globalStyles.container}>
            <Text style={globalStyles.logo}>iSpeak</Text>
            <View style={globalStyles.inputView} >
              <TextInput  
                style={globalStyles.inputText}
                placeholder="Login..." 
                placeholderTextColor="#003f5c"
                onChangeText={text => this.setState({login:text})}/>
            </View>
            <View style={globalStyles.inputView} >
              <TextInput  
                secureTextEntry
                style={globalStyles.inputText}
                placeholder="Password..." 
                placeholderTextColor="#003f5c"
                onChangeText={text => this.setState({password:text})}/>
            </View>
            <View style={globalStyles.inputView} >
              <TextInput  
                secureTextEntry
                style={globalStyles.inputText}
                placeholder="Repeat password..." 
                placeholderTextColor="#003f5c"
                onChangeText={text => this.setState({password:text})}/>
            </View>
            <TouchableOpacity style={styles.loginBtn} onPress={this.onPress}>
              <Text style={styles.loginText}>Sign Up</Text>
            </TouchableOpacity>
          </View>
        );
      }
    }
    
    const styles = StyleSheet.create({
      loginBtn:{
        width:"80%",
        backgroundColor:BUTTON_COLOR,
        borderRadius:25,
        height:50,
        alignItems:"center",
        justifyContent:"center",
        marginTop:20,
        marginBottom:10
      },
      loginText:{
        color:"white"
      }
    });